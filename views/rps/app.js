const choiceRock_img = document.getElementById("r");
const choicePaper_img = document.getElementById("p");
const choiceScissor_img = document.getElementById("s");
const refresh_img = document.getElementById("refresh");
const versus_div = document.querySelector(".versus");
const result_div = document.getElementsByClassName("result");

class Rps {
  constructor(userP) {
    this.userP = userP;
  }
  start() {
    function win(userChoice, computerChoice) {
      versus_div.classList.replace("versus", "result");
      document.getElementById("result-text").style.background = "#4c9654";
      document.getElementById("result-text").innerHTML = "player 1 Win";
    }
    function lose(userChoice, computerChoice) {
      versus_div.classList.replace("versus", "result");
      document.getElementById("result-text").style.background = "#4c9654";
      document.getElementById("result-text").innerHTML = "com Win";
    }
    function draw(userChoice, computerChoice) {
      versus_div.classList.replace("versus", "result");
      document.getElementById("result-text").style.background = "#035b0c";
      document.getElementById("result-text").innerHTML = "draw";
    }

    function getComputerChoice() {
      const choices = ["r", "p", "s"];
      const randomNumber = Math.floor(Math.random() * 3);
      return choices[randomNumber];
    }

    function game(userChoice) {
      const computerChoice = getComputerChoice();
      switch (userChoice + computerChoice) {
        case "rs":
        case "pr":
        case "sp":
          win();
          break;
        case "rp":
        case "ps":
        case "sr":
          lose();
          break;
        case "rr":
        case "pp":
        case "ss":
          draw();
          break;
      }
    }

    function main() {
      choiceRock_img.addEventListener("click", function () {
        game("r");
      });
      choicePaper_img.addEventListener("click", function () {
        game("p");
      });
      choiceScissor_img.addEventListener("click", function () {
        game("s");
      });
      refresh_img.addEventListener("click", function () {
        location.reload();
      });
    }
    main();
  }
}

const user1 = new Rps(prompt("Masukkan nama"));

user1.start();
