const http = require("http");
const fs = require("fs");
const url = require("url");
const onRequest = (request, response) => {
  console.log(request.url, request.method);

  let path = "./views/";
  switch (request.url) {
    case "/":
      path += "landingpage/index.html";
      response.statusCode = 200;
      break;
    case "/about":
      path += "about.html";
      response.statusCode = 200;
      break;
    case "/rps":
      path += "rps/index.html";
      response.statusCode = 200;
      break;
    case "/about-me":
      path += "index.html";
      response.statusCode = 301;
      return response.end;
    default:
      path += "404.html";
      response.statusCode = 404;
  }

  // Set header content type
  response.setHeader("Content-Type", "text/html");

  // Send an html file
  fs.readFile(path, null, (err, data) => {
    if (err) {
      response.writeHead(404);
      response.write("File Not Found");
    } else {
      response.write(data);
    }
    response.end();
  });
};
http.createServer(onRequest).listen(8000, "localhost", () => {
  console.log("Listening for request on port 8000");
});
